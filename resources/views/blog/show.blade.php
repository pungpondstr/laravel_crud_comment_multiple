@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Blog Show
                </div>

                <div class="card-body">
                    <form action="{{"/blog/$blogs->id"}}" method="post">
                        {{ csrf_field() }}
                        <input class='form-control mb-2' placeholder='title' name='title' value="{{$blogs->title}}" readonly/>
                        <textarea class='form-control mb-2' name="content" id="" cols="30" rows="10" placeholder="Content" readonly>
                            {{$blogs->content}}
                        </textarea>
                    </form>
                </div>
            </div>

            <div class="card mt-2">
                <div class="card-header" align='right'>
                    <b>Comment</b>
                </div>

                <div class="card-body">
                    <form action="{{"/comment/$blogs->id"}}" method="post">
                        {{ csrf_field() }}

                        <textarea class="form-control" name="message" id="" cols="30" rows="10" placeholder="Message"></textarea>
                        <div align='right'>
                            <input class='btn btn-primary mt-2' type='submit' value="Send"/>
                        </div>
                    </form>
                </div>
            </div>

            @foreach ($comments as $item)
            <div class="card mt-2">
                <div class="card-header" align='right'>
                    <b>{{$item->comments->name}}</b>
                </div>

                <div class="card-body">
                    <h4>{{$item->message}}</h4>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
