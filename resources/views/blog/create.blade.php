@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Blog Create
                </div>

                <div class="card-body">
                    <form action="{{"/blog"}}" method="post">
                        {{ csrf_field() }}
                        <input class='form-control mb-2' placeholder='title' name='title' required/>
                        <textarea class='form-control mb-2' name="content" id="" cols="30" rows="10" placeholder="Content" required></textarea>
                        <div align='right'>
                            <button class='btn btn-primary' type='submit'>Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
