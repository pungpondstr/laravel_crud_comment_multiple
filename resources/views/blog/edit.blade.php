@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Blog Edit
                </div>

                <div class="card-body">
                    <form action="{{"/blog/$blogs->id"}}" method="post">
                        {{ csrf_field() }}
                        @method('PUT')
                        <input class='form-control mb-2' placeholder='title' name='title' value="{{$blogs->title}}" required/>
                        <textarea class='form-control mb-2' name="content" id="" cols="30" rows="10" placeholder="Content" required>
                            {{$blogs->content}}
                        </textarea>
                        <div align='right'>
                            <button class='btn btn-primary' type='submit'>Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
