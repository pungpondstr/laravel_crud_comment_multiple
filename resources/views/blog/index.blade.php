@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6" align='left'>
                            Blog Index
                        </div>
                        <div class="col-6" align='right'>
                            <a class='btn btn-primary' href="/blog/create">Create</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class='table table-bordered'>
                        <tr align='center'>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>

                        @foreach ($blogs as $item)
                            <tr align='center'>
                                <td>
                                    <a href="{{"/blog/$item->id"}}">
                                        {{$item->title}}
                                    </a>
                                </td>
                                <td>
                                    <form action="{{"/blog/$item->id"}}" method="post">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <a class='btn btn-info' href="{{"/blog/$item->id/edit"}}">Edit</a>
                                        <button class='btn btn-danger' type='submit'>Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
