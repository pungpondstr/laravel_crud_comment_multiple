<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Comment extends Model
{
    protected $fillable = [
        'user_id',
        'blog_id',
        'message'
    ];

    public function comments()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
